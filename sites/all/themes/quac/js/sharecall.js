(function($) {
  $(function(){
    $("div").children('.views-responsive-grid-horizontal').each(function(parentIndex, parentItem) {
        var link_field = $(this).children().children().children('.views-field-field-link');
        var share_field = $(this).children().children().children('.views-field-nothing');
        var link_url = link_field.children().children('a').attr('href');

        share_field.share({
            url: link_url,
            button_text: 'Share',
            color: '#eb2293',
            background: '#ebebeb'
          });

    });
  });
})(jQuery);
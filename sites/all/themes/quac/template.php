<?php

/**
 * @file
 * template.php
 */

/**
 * Add the classes panel and panel-body to the main page content.
 * This will put the main page content in a rounded corner box
 * with a white background.
 *
 */
function quac_preprocess_region(&$variables, $hook) {
  if($variables['region'] == "content"){
    $variables['classes_array'][] = 'panel';
    $variables['classes_array'][] = 'panel-body';
  }
}


/**
 * Add the class conatiner to the footer-region block.
 * This will allow the footer to be full width, but
 * the footer content to be constrained by bootstraps
 * default width and margin settings.
 *
 */
function quac_preprocess_block(&$variables) {

}
